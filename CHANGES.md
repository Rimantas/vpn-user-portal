# Changelog

## 3.6.0 (2024-04-02)
- implement support for WireGuard over TCP/HTTP(S) 
  ([#189](https://todo.sr.ht/~eduvpn/server/189))

## 3.5.8 (2024-03-19)
- fix `AccessHook` restricting access to the service 
  ([CB#3](https://codeberg.org/eduVPN/vpn-user-portal/issues/3))
- fix unable to obtain "permissions" when using OIDC after logout 
  ([CB#3](https://codeberg.org/eduVPN/vpn-user-portal/issues/3))
- allow logout for disabled and unauthorized accounts 
  ([CB#4](https://codeberg.org/eduVPN/vpn-user-portal/issues/4))
- fix bug where disabled accounts were not disabled during first HTTP request 
  after logout/login cycle 
  ([CB#5](https://codeberg.org/eduVPN/vpn-user-portal/issues/5))

## 3.5.7 (2024-03-14)
- allow hiding VPN profiles ([#190](https://todo.sr.ht/~eduvpn/server/190))
- add `tr-TR` portal translation
- make table headers "sticky" when scrolling over tables in the portal
- enhance CLI "account" tool to delete "stale" VPN user accounts
- add CLI tool to calculate and optimize IP prefixes based on 
  including/excluding certain other prefixes
- reduce HTML header size of "Home" page

## 3.5.6 (2024-02-12)
- expose `vpn_proto_transport_list` in API `/info` response
- the last octet of generated IPv4 address will be 0 now (`generate-prefix`)
- add support for permissions based on client source IP 
  ([#146](https://todo.sr.ht/~eduvpn/server/146))

## 3.5.5 (2024-01-19)
- solve OpenVPN split tunnel overlap IP prefix issues for Windows 
  ([#185](https://todo.sr.ht/~eduvpn/server/185))

## 3.5.4 (2024-01-04)
- fix database query with PostgreSQL for enabling/disabling accounts 
  ([#PR2](https://codeberg.org/eduVPN/vpn-user-portal/pulls/2))
- implement "/user_configuration_list" Admin API call 
  ([#180](https://todo.sr.ht/~eduvpn/server/180))
- implement "/delete_user_authorizations" Admin API call 
  ([#184](https://todo.sr.ht/~eduvpn/server/184))

## 3.5.3 (2023-12-12)
- implement "Admin API" call `/connection_list` 
  ([#177](https://todo.sr.ht/~eduvpn/server/177))
- implement "Admin API" call `/delete_connection`
  ([#178](https://todo.sr.ht/~eduvpn/server/178))
- add script to generate random IPv4 and IPv6 prefixes

## 3.5.2 (2023-11-29)
- make it possible to restrict allowed OAuth clients 
  ([#170](https://todo.sr.ht/~eduvpn/server/170))
- add govVPN OAuth client registrations (disabled by default)
- implement `/users` and `/disabled_users` "Admin API" calls 
  ([#171](https://todo.sr.ht/~eduvpn/server/171))
- fix support for per user override of the session expiry
  ([#175](https://todo.sr.ht/~eduvpn/server/175))
- fix "Admin API" response of `/connected_users` call

## 3.5.1 (2023-11-13)
- update `cs-CZ` translation
- update `nl-NL` translation
- small fix in `de-DE` translation
- support resource capabilities for expressing session expiry (AARC-G027)
- implement `/connected_users` "Admin API" call
  ([#165](https://todo.sr.ht/~eduvpn/server/165))
- implement `/disable_user` and `/enable_user` "Admin API" calls
  ([#164](https://todo.sr.ht/~eduvpn/server/164))

## 3.5.0 (2023-10-30)
- improve syslog by allowing custom log format and additional variables 
  ([#162](https://todo.sr.ht/~eduvpn/server/162))
- simplify permission checking
- support permissions that have the attribute name in front of them
- modify authentication modules and static permission source to also include 
  the attribute name in their permissions
- add more unit tests
- make it possible to specify the attribute name for static permissions in the
  JSON file
- show the "Profile ID" on the "Info" page
- verify the `profileId` configuration field syntax
- implement support for "Live Permissions" 
  ([#131](https://todo.sr.ht/~eduvpn/server/131))

## 3.4.4 (2023-10-10)
- remove some unused code
- update translations
- make profile download options translatable

## 3.4.3 (2023-10-09)
- implement TLS support for MariaDB/MySQL 
  ([#159](https://todo.sr.ht/~eduvpn/server/159))
- add DNS search domains to API info response 
  ([#158](https://todo.sr.ht/~eduvpn/server/158))
- remove "Advanced" section for downloading VPN configuration files in portal,
  instead show all available options under "Profile"

## 3.4.2 (2023-10-02)
- use `fkooman/radius` for RADIUS support when the PHP radius extension is not 
  available (PHP >= 8) ([#139](https://todo.sr.ht/~eduvpn/server/139))
- add additional `redirect_uri` for the Android eduVPN/Let's Connect! apps 
  correcting earlier inconsistency
- make clear the CA on the "Info" page is for OpenVPN (only)
- update `uk-UA` translation

## 3.4.1 (2023-09-12)
- add `cs-CZ` translation
- add `sk-SK` translation
- fix two LDAP bugs ([#156](https://todo.sr.ht/~eduvpn/server/156))
  - restore support for `bindDnTemplate` format in common use with Active 
    Directory
  - switch to `ldap_read` when obtaining attributes of a DN instead of 
    performing a subtree search
- allow for writing `authData` to syslog 
  ([#140](https://todo.sr.ht/~eduvpn/server/140))

## 3.4.0 (2023-09-06)
- update `fkooman/oauth2-server` dependency (7.7.0, 7.8.0)
- allow search domains for VPN connections where all traffic is sent over the
  VPN ([#152](https://todo.sr.ht/~eduvpn/server/152))
- Added Catalan (`ca-ES`) translation for the user portal
- show unsupported configuration keys used in the configuration file
  ([#147](https://todo.sr.ht/~eduvpn/server/147))
- remove database query from `DisabledUserHook`
- refactor permission sources
- only fetch static permissions once during a session, not on every page load
- limit allowed OAuth scopes for the VPN clients to only `config`
- `ConnectionManager:oDisconnect` does not use `userId` parameter, remove it
- update translations of "Issues" in various languages
- LDAP improvements
  - always require `userIdAttribute` to be set ([announcement](https://lists.geant.org/sympa/arc/eduvpn-deploy/2023-08/msg00004.html))
  - add TLS configuration for the LDAP client, allow specifying CA, client 
    certificate and key ([#154](https://todo.sr.ht/~eduvpn/server/154))
- **EXPERIMENTAL** support to make it possible to configure WireGuard MTU 
  ([#151](https://todo.sr.ht/~eduvpn/server/151), 
  [documentation](https://docs.eduvpn.org/server/v3/wireguard.html#mtu))

## 3.3.6 (2023-05-23)
- consider allocated WireGuard IPs for alerting 
  ([#134](https://todo.sr.ht/~eduvpn/server/134))
- implement freeing WireGuard IPs for VPN clients that are unresponsive 
  ([#4](https://todo.sr.ht/~eduvpn/server/4))

## 3.3.5 (2023-05-11)
- no longer have a minimum value for `sessionExpiry`, previously it was `PT30M`
- move `StaticPermissionHook` functionality to `UpdateUserInfoHook`
- always fetch static permissions
- consolidate various `Storage::user*` methods
- allow node(s) to specify OpenVPN user/group
  ([#133](https://todo.sr.ht/~eduvpn/server/133))
- fix various static code analysis warnings
- `vpn-user-portal-status` now also shows the number of allocated IP addresses
  for WireGuard (and the number of still free addresses) 
  ([#4](https://todo.sr.ht/~eduvpn/server/4))
- do not show empty array when using `--alert` and `--json` with 
  `vpn-user-portal-status` and there is nothing to alert about

## 3.3.4 (2023-04-25)
- implement support for user specific "Session Expiry" 
  ([#88](https://todo.sr.ht/~eduvpn/server/88))

## 3.3.3 (2023-03-28)
- cleanup VPN protocol selection negotiation 
  ([#128](https://todo.sr.ht/~eduvpn/server/128), 
  [#113](https://todo.sr.ht/~eduvpn/server/113))

## 3.3.2 (2023-03-22)
- make `vpn-user-portal-account --list` also show local users when 
  `DbAuthModule` is used ([#125](https://todo.sr.ht/~eduvpn/server/125))

## 3.3.1 (2023-02-09)
- on "Info" page warn when DNS search domain is not set for a profile, while 
  DNS is provided, but not default gateway 
  ([#120](https://todo.sr.ht/~eduvpn/server/120))
- on "Info" page if DNS is not used in split-tunnel scenario do not warn when 
  DNS traffic is not sent over VPN
- switch to [Argon2id](https://en.wikipedia.org/wiki/Argon2) hashes for 
  local account passwords
- switch to new color palette for "App Usage" on "Stats" page
- show number of users on "Users" page
- expose `created_at` from `Storage::oCertList` and `Storage::wPeerList`
  ([#121](https://todo.sr.ht/~eduvpn/server/121))
- expose the max #available connections per profile on "Connections" page 
  ([#122](https://todo.sr.ht/~eduvpn/server/122))
- make it possible to add additional OAuth API clients 
  ([#119](https://todo.sr.ht/~eduvpn/server/119))
- switch session storage to use JSON instead of PHP serialization
  - this will log everyone out of the portal (if they are currently logged in),
    will NOT affect VPN sessions
- various fixes for issues found by security audit
  - DEC-02-004 WP1: Stored XSS via VPN-configuration display-name (High)
  - DEC-02-006 WP1: Stored XSS via null byte truncation in Radius auth (High)
  - DEC-02-007 WP1: Client disconnection via absent access control (Medium)
  - DEC-02-008 WP1: Bypassing connection threshold with race conditions (Low)
  - DEC-02-001 WP1: Trim function does not HTML-escape short strings (Medium)
  - DEC-02-005 WP3: Unnecessary use of unserialize() for cookie storage (Low)

## 3.3.0 (2023-01-20)
- do not write `syslog` output to `stderr` 
  ([#117](https://todo.sr.ht/~eduvpn/server/117))
- add "#Unique Guest Users" to the last week's "Stats"
- add "#Unique Guest Users" to the "Aggregated Stats"
- "Aggregated Stats" will now contain data starting "yesterday" instead of 
  "one week ago"
- Various database fixes
  - Fix long standing issue with MariaDB/MySQL with "Aggregate Stats" ([#53](https://todo.sr.ht/~eduvpn/server/53))
  - Fix PostgreSQL again with "Aggregate Stats" ([#118](https://todo.sr.ht/~eduvpn/server/118))
  - Add index on `connection_log` table to make generating "Aggregate Stats" 
    fast ([#112](https://todo.sr.ht/~eduvpn/server/112))
  - **NOTE**: a database 
    [migration](https://github.com/eduvpn/documentation/blob/v3/DATABASE.md#database-migration) 
    is necessary. This is done automatically with SQLite. If you switched to 
    using MariaDB/MySQL, or PostgreSQL you MUST do this manually! 

## 3.2.2 (2022-12-22)
- fix for [bug](https://github.com/eduvpn/apple/issues/487) in iOS/macOS app 
  regarding OAuth token refreshing after server upgrade from 2.x to 3.x

## 3.2.1 (2022-12-20)
- fix SQL query for exporting "Aggregate Stats"
- make log of adding/removing peers during sync more informative
- add name of server to aggregate/live stats file downloads

## 3.2.0 (2022-12-16)
- (re)implement tool to generate (reverse) DNS zone files
  ([#25](https://todo.sr.ht/~eduvpn/server/25))
- (re)implement "Static Permissions" for cases where your authentication 
  backend does not (adequately)
  ([#18](https://todo.sr.ht/~eduvpn/server/18)) 
- update for vpn-daemon `/w/remove_peer` changes (v3.0.2)
- add some tests to verify `nodeNumber`, `nodeUrl` and `onNode` profile 
  configuration file
- show `nodeNumber` on Info page for the node(s)
- add `LoggerInterface::debug`
- remove `Tpl::profileIdToDisplayName` "cache"
- refactor connect/disconnect event hooks
- write to `connection_log` table from `ConnectionLogHook`
- make `VPN_PROTO` available on connect/disconnect in `ScriptConnectionHook`
- make `VPN_BYTES_IN` and `VPN_BYTES_OUT` available on disconnect  in 
  `ScriptConnectionHook`
- cleanup "daemon-sync" to make sure the correct connect/disconnect events are
  triggered in all cases
- make "daemon-sync" delete certificates/peers that no longer match the 
  configuration on "apply changes" 
  ([#96](https://todo.sr.ht/~eduvpn/server/96))
- try all nodes when attempting to connect with WireGuard and the first node 
  ran out of free IP addresses ([#110](https://todo.sr.ht/~eduvpn/server/110))
- fix "Aggregate Stats" inefficient `LEFT JOIN` query
  ([#112](https://todo.sr.ht/~eduvpn/server/112))
- sort/group "Aggregate Stats"

## 3.1.7 (2022-11-18)
- fix `ConfigCheck` with DNS template variables 
  ([#107](https://todo.sr.ht/~eduvpn/server/107))
- add network prefix to `AllowedIPs` by default for WireGuard client 
  configuration ([#108](https://todo.sr.ht/~eduvpn/server/108)) 

## 3.1.6 (2022-11-17)
- enforce format of remote user IDs for guest users 
  ([#104](https://todo.sr.ht/~eduvpn/server/104))
- restore `@GW4@` and `@GW6@` template variables for `dnsServerList`
  ([#105](https://todo.sr.ht/~eduvpn/server/105))

## 3.1.5 (2022-11-11)
- fix application stats on "Stats" admin page 
  ([#102](https://todo.sr.ht/~eduvpn/server/102))
- prevent *local* revoked clients from using API in "Guest Usage" scenario 
  ([#103](https://todo.sr.ht/~eduvpn/server/103))

## 3.1.4 (2022-11-08)
- fix OpenVPN special port handling 
  ([#101](https://todo.sr.ht/~eduvpn/server/101))

## 3.1.3 (2022-11-07)
- fix (C) year
- cast `ini_get` return value for `mbstring.func_overload` to bool

## 3.1.2 (2022-11-07)
- make sure `mbstring.func_overload` PHP option is not enabled, show on "Info"
  page if it is
- do proper UTF-8 validation and introduce maximum length of some user provided 
  inputs

## 3.1.1 (2022-11-04)
- verify and trim node keys before allowing them 
  ([#100](https://todo.sr.ht/~eduvpn/server/100))
- fix `nb-NO` translation typo

## 3.1.0 (2022-10-24)
- fix warning message for non-https node URL 
  ([#93](https://todo.sr.ht/~eduvpn/server/93))
- update `nl-NL` translation
- update for `fkooman/oauth2-server` 7.1
- introduce `ApiUserInfo` that wraps the OAuth access token
- enable `iss` query parameter support for OAuth callbacks with 
  `fkooman/oauth2-server` 7.2 ([#91](https://todo.sr.ht/~eduvpn/server/91))
- implement 
  [Guest Access](https://github.com/eduvpn/documentation/blob/v3/GUEST_ACCESS.md) 
  support ([#17](https://todo.sr.ht/~eduvpn/server/17))
  - implement `HmacUserIdHook` to obscure user IDs 
    ([#89](https://todo.sr.ht/~eduvpn/server/89))
  - add [minisign](https://jedisct1.github.io/minisign/) compatible 
    signature verifier
  
## 3.0.6 (2022-09-19)
- [PREVIEW](https://github.com/eduvpn/documentation/blob/v3/PREVIEW_FEATURES.md): 
  implement "Admin API" support ([#16](https://todo.sr.ht/~eduvpn/server/16))
- fix multi node deployments when profile is not installed on all nodes 
  ([#90](https://todo.sr.ht/~eduvpn/server/90))
- simplify `.well-known` handling code in development setup
- add additional `ProfileConfig` tests
- add simple shell script client `dev/api_client.sh` for API testing /
  development

## 3.0.5 (2022-08-15)
- [PREVIEW](https://github.com/eduvpn/documentation/blob/v3/PREVIEW_FEATURES.md): 
  add support for deleting authorization on APIv3 disconnect call 
  ([#78](https://todo.sr.ht/~eduvpn/server/78))

## 3.0.4 (2022-08-03)
- fix handling optional `oListenOn` in multi node setups 
  ([#85](https://todo.sr.ht/~eduvpn/server/85))
- implement `ConnectionHookInterface` to allow for plugins to respond to client
  connect/disconnect events ([#82](https://todo.sr.ht/~eduvpn/server/82))
- re-implement the _syslog_ connection logger on top of 
  `ConnectionHookInterface`
- implement `--list` option for `vpn-user-portal-account` to list user accounts
- [PREVIEW](https://github.com/eduvpn/documentation/blob/v3/PREVIEW_FEATURES.md): 
  add support for running script/command on client connect/disconnect 
  ([#84](https://todo.sr.ht/~eduvpn/server/84))
  
## 3.0.3 (2022-07-27)
- proper logging of authentication failures for local database, LDAP and RADIUS

## 3.0.2 (2022-07-25)
- add Portal URL to manually downloaded configuration file ([#81](https://todo.sr.ht/~eduvpn/server/81))
- update `ar-MA` translation
- require userIdAttribute to be set in LDAP response when requesting it to be
  used ([#83](https://todo.sr.ht/~eduvpn/server/83))

## 3.0.1 (2022-06-08)
- make `oListenOn` accept multiple values 
  ([#75](https://todo.sr.ht/~eduvpn/server/75))
- update `pt-PT` translation
- update `ar-MA` translation

## 3.0.0 (2022-05-18)
- initial 3.x release
