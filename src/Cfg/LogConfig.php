<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Cfg;

class LogConfig
{
    use ConfigTrait;

    private array $configData;

    public function __construct(array $configData)
    {
        $this->configData = $configData;
    }

    public function syslogConnectionEvents(): bool
    {
        return $this->requireBool('syslogConnectionEvents', false);
    }

    public function originatingIp(): bool
    {
        return $this->requireBool('originatingIp', false);
    }

    public function authData(): bool
    {
        return $this->requireBool('authData', false);
    }

    public function connectLogTemplate(): string
    {
        if (null !== $connectLogTemplate = $this->optionalString('connectLogTemplate')) {
            return $connectLogTemplate;
        }

        if ($this->authData()) {
            return 'CONNECT {{USER_ID}} ({{PROFILE_ID}}:{{CONNECTION_ID}}) [{{ORIGINATING_IP}} => {{IP_FOUR}},{{IP_SIX}}] [AUTH_DATA={{AUTH_DATA}}]';
        }

        return 'CONNECT {{USER_ID}} ({{PROFILE_ID}}:{{CONNECTION_ID}}) [{{ORIGINATING_IP}} => {{IP_FOUR}},{{IP_SIX}}]';
    }

    public function disconnectLogTemplate(): string
    {
        if (null !== $disconnectLogTemplate = $this->optionalString('disconnectLogTemplate')) {
            return $disconnectLogTemplate;
        }

        return 'DISCONNECT {{USER_ID}} ({{PROFILE_ID}}:{{CONNECTION_ID}})';
    }
}
