HTTP_PORT ?= 8082

.PHONY: test fmt psalm phpstan sloc dev

test:
	# try system wide installations of PHPUnit first
	/usr/bin/phpunit || /usr/bin/phpunit9 || phpunit

fmt:
	php-cs-fixer fix

psalm:
	psalm

phpstan:
	phpstan

sloc:
	phploc src web libexec bin

dev:
	@php -S localhost:$(HTTP_PORT) -t web dev/router.php
