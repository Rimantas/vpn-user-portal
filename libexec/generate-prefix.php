<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

$showFour = false;
$showSix = false;
$prefixCount = 1;

for ($i = 1; $i < $argc; $i++) {
    if ('-4' === $argv[$i]) {
        $showFour = true;
    }
    if ('-6' === $argv[$i]) {
        $showSix = true;
    }
    if ('-n' === $argv[$i]) {
        if ($i + 1 < $argc) {
            $pC = (int) $argv[++$i];
            if ($pC >= 0) {
                $prefixCount = $pC;
            }
        }

        continue;
    }
    if ('-h' === $argv[$i] || '--help' === $argv[$i]) {
        echo 'SYNTAX: ' . $argv[0] . ' [-4] [-6] [-n COUNT]' . PHP_EOL;
        exit(0);
    }
}

// if nothing is explicity requested, show both IPv4 and IPv6 prefix
if (!$showFour && !$showSix) {
    $showFour = $showSix = true;
}

for ($i = 0; $i < $prefixCount; $i++) {
    if ($showFour) {
        echo '10.' . ord(random_bytes(1)) . '.' . ord(random_bytes(1)) . '.0/24' . PHP_EOL;
    }
    if ($showSix) {
        echo chunk_split('fd' . bin2hex(random_bytes(7)), 4, ':') . ':/64' . PHP_EOL;
    }
}
