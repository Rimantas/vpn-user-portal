<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use Vpn\Portal\Json;
use Vpn\Portal\TplInterface;

class TestTpl implements TplInterface
{
    /** @var array<string, mixed> */
    private array $tplVariables = [];

    /**
     * @param mixed $v
     */
    public function addDefault(string $k, $v): void
    {
        $this->tplVariables[$k] = $v;
    }

    /**
     * @param array<string,mixed> $templateVariables
     */
    public function render(string $templateName, array $templateVariables = []): string
    {
        return Json::encode(
            [
                $templateName => array_merge($this->tplVariables, $templateVariables),
            ]
        );
    }
}
