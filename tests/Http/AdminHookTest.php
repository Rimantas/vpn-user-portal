<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Http\AdminHook;
use Vpn\Portal\Http\Request;
use Vpn\Portal\Http\UserInfo;

/**
 * @coversNothing
 */
class AdminHookTest extends TestCase
{
    public function testNoAdmin(): void
    {
        $tpl = new TestTpl();
        $a = new AdminHook(['xyz'], ['bar'], $tpl);
        $u = new UserInfo('foo', ['p1', 'p2']);
        $this->assertNull($a->afterAuth(new Request([], [], [], []), $u));
        $this->assertFalse($u->isAdmin());
        $this->assertSame('{"foo":[]}', $tpl->render('foo', []));
    }

    public function testAdminPermissionList(): void
    {
        $tpl = new TestTpl();
        $a = new AdminHook(['p1'], ['bar'], $tpl);
        $u = new UserInfo('foo', ['p1', 'p2']);
        $this->assertNull($a->afterAuth(new Request([], [], [], []), $u));
        $this->assertTrue($u->isAdmin());
        $this->assertSame('{"foo":{"isAdmin":true}}', $tpl->render('foo', []));
    }

    public function testAdminUserId(): void
    {
        $tpl = new TestTpl();
        $a = new AdminHook(['xyz'], ['foo'], $tpl);
        $u = new UserInfo('foo', ['p1', 'p2']);
        $this->assertNull($a->afterAuth(new Request([], [], [], []), $u));
        $this->assertTrue($u->isAdmin());
        $this->assertSame('{"foo":{"isAdmin":true}}', $tpl->render('foo', []));
    }
}
