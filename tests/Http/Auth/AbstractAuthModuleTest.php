<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Http\Auth\AbstractAuthModule;

/**
 * @coversNothing
 */
class AbstractAuthModuleTest extends TestCase
{
    public function testFlattenPermissionList(): void
    {
        $this->assertSame(
            [
                'n1!v1',
                'n1!v2',
                'n2!v3',
                'n2!v4',
            ],
            AbstractAuthModule::flattenPermissionList(
                [
                    'n1' => ['v1', 'v2'],
                    'n2' => ['v3', 'v4'],
                ],
                ['n1', 'n2']
            )
        );
    }

    public function testFlattenPermissionListSubset(): void
    {
        $this->assertSame(
            [
                'n1!v1',
                'n1!v2',
            ],
            AbstractAuthModule::flattenPermissionList(
                [
                    'n1' => ['v1', 'v2'],
                    'n2' => ['v3', 'v4'],
                ],
                ['n1']
            )
        );
    }

    public function testFlattenPermissionListMissing(): void
    {
        $this->assertSame(
            [
                'n1!v1',
                'n1!v2',
            ],
            AbstractAuthModule::flattenPermissionList(
                [
                    'n1' => ['v1', 'v2'],
                    'n2' => ['v3', 'v4'],
                ],
                ['n1', 'n3']
            )
        );
    }

    public function testFlattenPermissionListNone(): void
    {
        $this->assertSame(
            [
            ],
            AbstractAuthModule::flattenPermissionList(
                [
                    'n1' => ['v1', 'v2'],
                    'n2' => ['v3', 'v4'],
                ],
                ['n4', 'n5']
            )
        );
    }
}
