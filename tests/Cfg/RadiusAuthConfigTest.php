<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Cfg\RadiusAuthConfig;

/**
 * @coversNothing
 */
final class RadiusAuthConfigTest extends TestCase
{
    public function testIpFour(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    '127.0.0.1:1812:s3cr3t',
                ],
            ]
        );
        $this->assertSame(
            [
                [
                    'radiusHost' => '127.0.0.1',
                    'radiusPort' => 1812,
                    'radiusSecret' => 's3cr3t',
                ],
            ],
            $r->serverList()
        );
    }

    public function testIpSix(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    '[fdd4:56b7:5ebc:4e82::1]:1812:s3cr3t',
                ],
            ]
        );
        $this->assertSame(
            [
                [
                    'radiusHost' => '[fdd4:56b7:5ebc:4e82::1]',
                    'radiusPort' => 1812,
                    'radiusSecret' => 's3cr3t',
                ],
            ],
            $r->serverList()
        );
    }

    public function testMultiple(): void
    {
        $r = new RadiusAuthConfig(
            [
                'serverList' => [
                    'radius.example.org:12345:foobar',
                    '127.0.0.1:1812:s3cr3t',
                    '[fdd4:56b7:5ebc:4e82::1]:1812:s3cr3t',
                ],
            ]
        );
        $this->assertSame(
            [
                [
                    'radiusHost' => 'radius.example.org',
                    'radiusPort' => 12345,
                    'radiusSecret' => 'foobar',
                ],
                [
                    'radiusHost' => '127.0.0.1',
                    'radiusPort' => 1812,
                    'radiusSecret' => 's3cr3t',
                ],
                [
                    'radiusHost' => '[fdd4:56b7:5ebc:4e82::1]',
                    'radiusPort' => 1812,
                    'radiusSecret' => 's3cr3t',
                ],
            ],
            $r->serverList()
        );
    }
}
