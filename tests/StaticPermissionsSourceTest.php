<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use PHPUnit\Framework\TestCase;
use Vpn\Portal\Cfg\StaticPermissionsConfig;
use Vpn\Portal\Http\Request;
use Vpn\Portal\StaticPermissionsSource;

/**
 * @internal
 *
 * @coversNothing
 */
final class StaticPermissionsSourceTest extends TestCase
{
    public function testRemoteAddressFour(): void
    {
        // inside the range
        $permissionSource = new StaticPermissionsSource(self::cfg(), self::remoteAddressRequest('192.168.5.5'));
        $this->assertSame(['memberOf!ipv4', 'memberOf!allowed-ip-list'], $permissionSource->permissionsForUser('x'));

        // outside the range
        $permissionSource = new StaticPermissionsSource(self::cfg(), self::remoteAddressRequest('192.168.6.5'));
        $this->assertSame(['memberOf!ipv4'], $permissionSource->permissionsForUser('x'));
    }

    public function testRemoteAddressSix(): void
    {
        // inside the range
        $permissionSource = new StaticPermissionsSource(self::cfg(), self::remoteAddressRequest('fd00::5'));
        $this->assertSame(['memberOf!ipv6', 'memberOf!allowed-ip-list'], $permissionSource->permissionsForUser('x'));

        // outside the range
        $permissionSource = new StaticPermissionsSource(self::cfg(), self::remoteAddressRequest('fd01::5'));
        $this->assertSame(['memberOf!ipv6',], $permissionSource->permissionsForUser('x'));
    }

    private static function cfg(): StaticPermissionsConfig
    {
        return new StaticPermissionsConfig(['baseDir' => __DIR__]);
    }

    private static function remoteAddressRequest(string $remoteAddress): Request
    {
        return new Request(
            [
                'REMOTE_ADDR' => $remoteAddress,
            ],
            [],
            [],
            []
        );
    }
}
