<?php

declare(strict_types=1);

/*
 * eduVPN - End-user friendly VPN.
 *
 * Copyright: 2014-2023, The Commons Conservancy eduVPN Programme
 * SPDX-License-Identifier: AGPL-3.0+
 */

namespace Vpn\Portal\Tests;

use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Vpn\Portal\Cfg\DbConfig;
use Vpn\Portal\Cfg\LogConfig;
use Vpn\Portal\Http\UserInfo;
use Vpn\Portal\LogConnectionHook;
use Vpn\Portal\Storage;

/**
 * @coversNothing
 */
class LogConnectionHookTest extends TestCase
{
    public function testConnect(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $testLogger = new TestLogger();
        $l = new LogConnectionHook($storage, $testLogger, new LogConfig([]));
        $l->connect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', null);
        $this->assertSame(
            [
                '[I] CONNECT user_id (profile_id:connection_id) [* => ip_four,ip_six]',
            ],
            $testLogger->getAll()
        );
    }

    public function testConnectOriginatingIp(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $testLogger = new TestLogger();
        $l = new LogConnectionHook($storage, $testLogger, new LogConfig(['originatingIp' => true]));
        $l->connect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', 'originating_ip');
        $this->assertSame(
            [
                '[I] CONNECT user_id (profile_id:connection_id) [originating_ip => ip_four,ip_six]',
            ],
            $testLogger->getAll()
        );
    }

    public function testConnectAuthDataYesAuthData(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $storage->userAdd(new UserInfo('user_id', [], 'auth_data'), new DateTimeImmutable());
        $testLogger = new TestLogger();
        $l = new LogConnectionHook($storage, $testLogger, new LogConfig(['authData' => true]));
        $l->connect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', null);
        $this->assertSame(
            [
                '[I] CONNECT user_id (profile_id:connection_id) [* => ip_four,ip_six] [AUTH_DATA=YXV0aF9kYXRh]',
            ],
            $testLogger->getAll()
        );
    }

    public function testConnectAuthDataNoAuthData(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $testLogger = new TestLogger();
        $l = new LogConnectionHook($storage, $testLogger, new LogConfig(['authData' => true]));
        $l->connect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', null);
        $this->assertSame(
            [
                '[I] CONNECT user_id (profile_id:connection_id) [* => ip_four,ip_six] [AUTH_DATA=]',
            ],
            $testLogger->getAll()
        );
    }

    public function testDisconnect(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $testLogger = new TestLogger();
        $l = new LogConnectionHook($storage, $testLogger, new LogConfig([]));
        $l->disconnect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', 12345, 54321);
        $this->assertSame(
            [
                '[I] DISCONNECT user_id (profile_id:connection_id)',
            ],
            $testLogger->getAll()
        );
    }

    public function testConnectTemplate(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $storage->userAdd(new UserInfo('user_id', [], 'auth_data'), new DateTimeImmutable());
        $testLogger = new TestLogger();
        $l = new LogConnectionHook(
            $storage,
            $testLogger,
            new LogConfig(
                [
                    'originatingIp' => true,
                    'connectLogTemplate' => 'C,{{USER_ID}},{{PROFILE_ID}},{{VPN_PROTO}},{{CONNECTION_ID}},{{IP_FOUR}},{{IP_SIX}},{{ORIGINATING_IP}},{{AUTH_DATA}}',
                ]
            )
        );
        $l->connect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', 'originating_ip');
        $this->assertSame(
            [
                '[I] C,user_id,profile_id,vpn_proto,connection_id,ip_four,ip_six,originating_ip,YXV0aF9kYXRh',
            ],
            $testLogger->getAll()
        );
    }

    public function testDisconnectTemplate(): void
    {
        $storage = new Storage(new DbConfig(['dbDsn' => 'sqlite::memory:', 'baseDir' => dirname(__DIR__)]));
        $storage->userAdd(new UserInfo('user_id', [], 'auth_data'), new DateTimeImmutable());
        $testLogger = new TestLogger();
        $l = new LogConnectionHook(
            $storage,
            $testLogger,
            new LogConfig(
                [
                    'originatingIp' => true,
                    'disconnectLogTemplate' => 'D,{{USER_ID}},{{PROFILE_ID}},{{VPN_PROTO}},{{CONNECTION_ID}},{{IP_FOUR}},{{IP_SIX}},{{BYTES_IN}},{{BYTES_OUT}},{{AUTH_DATA}}',
                ]
            )
        );
        $l->disconnect('user_id', 'profile_id', 'vpn_proto', 'connection_id', 'ip_four', 'ip_six', 12345, 54321);
        $this->assertSame(
            [
                '[I] D,user_id,profile_id,vpn_proto,connection_id,ip_four,ip_six,12345,54321,YXV0aF9kYXRh',
            ],
            $testLogger->getAll()
        );
    }
}
